Rails.application.routes.draw do
  devise_for :users, path: '',
                     path_names: { sign_in: 'entrar',
                                   sign_out: 'sair',
                                   sign_up: 'cadastro' }

  root 'projects#index'

  resources :clients

  resources :contacts

  resources :tasks

  resources :timesheets

  resources :expenses
  
  resources :teams

  resources :reports

  resources :projects do
    post 'archive',   to: 'projects#archive_project',   as: :archive
    post 'unarchive', to: 'projects#unarchive_project', as: :unarchive
  end
end

# Tundra Time Tracker

* Projeto pessoal para Gerenciamento de Tarefas e Controle de Horas Trabalhadas
* Work in Progress :rocket: 


# Dependências

* Ruby 2.6.3
* Rails 6
* PostgreSQL


Para instalar o Ruby ([RVM](https://rvm.io/) ou [Rbenv](https://github.com/rbenv/rbenv)) e o [PostgreSQL](https://www.postgresql.org/), utilize este [guia de instalação](https://gorails.com/setup).


## Configurando Projeto

Instalar as dependências através do comando:

* `bundle install`

Criar o banco de dados e `migrations`através do comando:

* `rails db:create db:migrate`

Popular com os dados de teste com o comando:

* `rails db:seed`

O `seed` vai criar 1 usuários de teste:

**User:**

```
email: test@test.com
password: 123456
```


Iniciar o servidor através do comando:


```
rails s

```

URL Padrão:


```
http://localhost:3000

```

# Demonstração

![tundra_01](https://i.imgur.com/haCVivL.gif)

---

![tundra_02](https://i.imgur.com/HKO423T.gif)

---

![tundra_03](https://i.imgur.com/rtj5n3e.gif)

---

![tundra_04](https://i.imgur.com/92kE8Qz.gif)

---

![tundra_05](https://i.imgur.com/DTGe1Co.gif)

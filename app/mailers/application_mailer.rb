class ApplicationMailer < ActionMailer::Base
  default from: 'contato@tundra-tracker.com'
  layout 'mailer'
end

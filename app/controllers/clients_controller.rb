class ClientsController < ApplicationController
  before_action :authenticate_user!

  before_action :set_client, only: %i[edit update destroy]

  def index
    @clients = current_user.clients
  end

  def new
    @client = Client.new
  end

  def create
    @client = Client.new(client_params)
    if @client.save
      redirect_to clients_path, notice: I18n.t('controllers.clients.create')
    else
      render :new
    end
  end

  def edit
    ###
  end

  def update
    if @client.update(client_params)
      redirect_to clients_path, notice: I18n.t('controllers.clients.update', client_name: @client.name)
    else
      render :edit
    end
  end

  def destroy
    @client.destroy
    redirect_to clients_path, alert: I18n.t('controllers.clients.delete', client_name: @client.name)
  end

  private

  def set_client
    @client = current_user.clients.find(params[:id])
  end

  def client_params
    params.require(:client).permit(:name, :currency_type, :address).merge(user: current_user)
  end
end

class ProjectsController < ApplicationController
  before_action :authenticate_user!

  before_action :set_project, only: %i[edit update show destroy]

  def index
    @projects = current_user.projects.unarchived
  end

  def new
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)
    if @project.save
      redirect_to projects_path, notice: I18n.t('controllers.projects.create')
    else
      render :new
    end
  end

  def edit
    ####
  end

  def update
    if @project.update(project_params)
      redirect_to projects_path, notice: I18n.t('controllers.projects.update')
    else
      render :edit
    end
  end

  def show
  end

  def destroy
    @project.destroy
    redirect_to projects_url, alert: I18n.t('controllers.projects.delete')
  end

  def archive_project
    project = current_user.projects.find(params["project_id"].to_i)
    project.update(archived: true) if project.present?
    redirect_to projects_path, notice: I18n.t('controllers.projects.archive')
  end

  def unarchive_project
    project = current_user.projects.find(params["project_id"].to_i)
    project.update(archived: false) if project.present?
    redirect_to projects_path, notice: I18n.t('controllers.projects.unarchive')
  end

  private

  def set_project
    @project = current_user.projects.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:name,
                                    :project_code,
                                    :starts_on,
                                    :ends_on,
                                    :total_hours,
                                    :description,
                                    :public,
                                    :client_id,
                                  ).merge(user: current_user)
  end
end

class ContactsController < ApplicationController
  before_action :authenticate_user!

  before_action :set_contact, only: %i[edit update destroy]

  def index
    @contacts = current_user.contacts
  end

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      redirect_to contacts_path, notice: I18n.t('controllers.contacts.create')
    else
      render :new
    end
  end

  def edit
    ###
  end

  def update
    if @contact.update(contact_params)
      redirect_to contacts_path, notice: I18n.t('controllers.contacts.update')
    else
      render :edit
    end
  end

  def destroy
    @contact.destroy
    redirect_to contacts_url, alert: I18n.t('controllers.contacts.delete')
  end

  private

  def set_contact
    @contact = current_user.contacts.find(params[:id])
  end

  def contact_params
    params.require(:contact).permit(:first_name, :last_name, :email, :office_number, :mobile_number, :client_id).merge(user: current_user)
  end
end

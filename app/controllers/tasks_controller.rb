class TasksController < ApplicationController
  before_action :authenticate_user!

  before_action :set_task, only: %i[edit update destroy]

  def index
    @tasks = Task.all
  end

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params)
    if @task.save
      redirect_to tasks_path, notice: I18n.t('controllers.tasks.create')
    else
      render :new
    end
  end

  def edit
    ###
  end

  def update
    if @task.update(task_params)
      redirect_to tasks_path, notice: I18n.t('controllers.tasks.update')
    else
      render :edit
    end
  end

  def destroy
    @task.destroy
    redirect_to tasks_url, alert: I18n.t('controllers.tasks.delete')
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:name, :hourly_rate, :billable).merge(user: current_user)
  end
end

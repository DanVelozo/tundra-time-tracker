class TimesheetsController < ApplicationController
  before_action :authenticate_user!

  before_action :set_timesheet, only: %i[edit update show destroy]

  def index
    @timesheets = current_user.timesheets.page(params[:page]).per(10)
  end

  def new
    @timesheet = Timesheet.new
  end

  def create
    @timesheet = Timesheet.new(timesheet_params)
    if @timesheet.save
      redirect_to timesheets_path, notice: I18n.t('controllers.timesheets.create')
    else
      render :new
    end
  end

  def edit
    # TODO
  end

  def update
    if @timesheet.update(timesheet_params)
      redirect_to timesheets_path, notice: I18n.t('controllers.timesheets.update')
    else
      render :edit
    end
  end

  def show
  end

  def destroy
    @timesheet.destroy
    redirect_to timesheets_path, alert: I18n.t('controllers.timesheets.delete')
  end

  private

  def set_timesheet
    @timesheet = current_user.timesheets.find(params[:id])
  end

  def timesheet_params
    params.require(:timesheet).permit(:description,
                                      :start_date,
                                      :work_from,
                                      :work_to,
                                      :duration,
                                      :external_url,
                                      :project_id,
                                      :task_id,
                                     ).merge(user: current_user)
  end
end

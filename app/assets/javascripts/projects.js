// Making table row into a link
$(document).ready(function () {
  $("tr[data-link]").click(function() {
    window.location = $(this).data("link")
  })
});

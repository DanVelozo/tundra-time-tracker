// Alert Timeout
$(document).ready(function () {
  window.setTimeout(function() {
      $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
          $(this).remove();
      });
  }, 3000);
});

// Datepicker configuration
$(document).ready(function () {
  $('.custom-datepicker').datepicker({
      format: 'dd/mm/yyyy',
      language: 'pt-BR',
    	todayHighlight: true,
      autoclose: true
  });
});

// Masks
$(document).ready(function ($) {
  $('.time-mask').mask('00:00');
});

$(document).ready(function () {
  $('#timesheet-end-time').on('change',function() {
    var start_actual_time = $('#timesheet-start-time').val();
    var end_actual_time   = $('#timesheet-end-time').val();

    start_actual_time = new Date("1970-1-1 " + start_actual_time);
    end_actual_time   = new Date("1970-1-1 " + end_actual_time);

    var diff = end_actual_time - start_actual_time;

    var diffSeconds = diff/1000;
    var HH = Math.floor(diffSeconds/3600);
    var MM = Math.floor(diffSeconds%3600)/60;

    var formatted = ((HH < 10)?("0" + HH):HH) + ":" + ((MM < 10)?("0" + MM):MM)

    $('#timesheet-duration').val(formatted);
  });
});

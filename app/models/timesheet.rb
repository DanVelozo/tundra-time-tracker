class Timesheet < ApplicationRecord
  belongs_to :project
  belongs_to :task
  belongs_to :user

  validates :description, presence: true
  validates :start_date, presence: true
end

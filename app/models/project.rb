class Project < ApplicationRecord
  belongs_to :client
  belongs_to :user
  has_many :timesheets, dependent: :destroy

  validates :name, presence: true
  validates :total_hours, presence: true

  scope :archived,   -> { where(archived: true) }
  scope :unarchived, -> { where(archived: false) }
end

class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable,
         :trackable, :validatable

  validates :first_name, presence: true
  validates :last_name,  presence: true
  validates :email,      presence: true, format: /\w+@\w+\.{1}[a-zA-Z]{2,}/

  has_many :projects,   dependent: :destroy
  has_many :clients,    dependent: :destroy
  has_many :contacts,   dependent: :destroy
  has_many :tasks,      dependent: :destroy
  has_many :timesheets, dependent: :destroy
end

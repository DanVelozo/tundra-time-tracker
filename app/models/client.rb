class Client < ApplicationRecord
  CURRENCY_TYPE = ['R$ BRL', '$ USD', '€ EUR'].freeze

  has_many :contacts, dependent: :destroy
  has_many :projects, dependent: :destroy

  belongs_to :user

  validates :name, presence: true
  validates :currency_type, presence: true
end

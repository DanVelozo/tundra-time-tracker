class Task < ApplicationRecord
  has_many :timesheets
  belongs_to :user
  validates :name, presence: true
  validates :billable, presence: true

  scope :get_billable, -> { where(billable: true) }
end

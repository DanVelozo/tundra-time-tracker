require 'rails_helper'

RSpec.describe Project, type: :model do
  let!(:user)  { FactoryBot.create(:user) }
  let(:client) { FactoryBot.create(:client, user: user) }

  describe 'factory' do
    it 'has a valid factory' do
      expect(FactoryBot.build(:project)).to be_valid
    end
  end

  describe 'associations' do
    it { should have_many(:timesheets) }
    it { should belong_to(:client) }
    it { should belong_to(:user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:total_hours) }
  end

  describe 'Attributes' do
    it 'is valid with a name, total_hours, user and client' do
      project = Project.new(
        name: 'Projeto XPTO',
        project_code: 'xpto2020',
        starts_on: '15/05/2020',
        total_hours: '160',
        user: user,
        client: client
      )
      
      expect(project).to be_valid
    end

    it 'is not valid without a name and total_hours' do
      project = Project.new(
        name: nil,
        project_code: 'xpto2020',
        starts_on: '15/05/2020',
        total_hours: nil,
        user: user,
        client: client
      )

      expect(project).to_not be_valid
    end
  end
end
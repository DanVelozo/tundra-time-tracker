require 'rails_helper'

RSpec.describe User, type: :model do

  describe 'factory' do
    it 'has a valid factory' do
      expect(FactoryBot.build(:user)).to be_valid
    end
  end

  describe 'associations' do
    it { should have_many(:projects) }
    it { should have_many(:clients) }
    it { should have_many(:contacts) }
    it { should have_many(:tasks) }
    it { should have_many(:timesheets) }
  end

  describe 'validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_presence_of(:email) }

    it { should_not allow_value("emailquenaoexiste").for(:email) }
  end

  describe 'Attributes' do
    it 'is valid with a first_name, last_name, email and password' do
      user = User.new(
        first_name: 'Danilo',
        last_name: 'Velozo',
        email: 'danilo@teste.com',
        password: '123456'
      )
      expect(user).to be_valid
    end

    it 'is not valid without a first_name, last_name and email' do
      user = User.new(
        first_name: nil,
        last_name: nil,
        email: nil,
        password: '123456'
      )
      expect(user).to_not be_valid
    end
  end
end

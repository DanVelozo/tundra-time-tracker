require 'rails_helper'

RSpec.describe Client, type: :model do
  let(:user) { FactoryBot.create(:user) }

  describe 'factory' do
    it 'has a valid factory' do
      expect(FactoryBot.build(:client)).to be_valid
    end
  end

  describe 'associations' do
    it { should belong_to(:user) }
    it { should have_many(:contacts) }
    it { should have_many(:projects) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:currency_type) }
  end

  describe 'Attributes' do
    it 'is valid with a name, currency_type and address' do
      client = Client.new(
        name: 'Cliente XPTO',
        currency_type: Client::CURRENCY_TYPE.first,
        address: 'Rua dos Testes',
        user: user
      )
      expect(client).to be_valid
    end

    it 'is not valid without a name' do
      client = Client.new(name: nil)
      expect(client).to_not be_valid
    end

    it 'is not valid without current_type' do
      client = Client.new(
        name: 'Cliente XYZ',
        currency_type: nil,
        address: 'Rua dos Testes',
        user: user
      )
      expect(client).to_not be_valid
    end
  end
end

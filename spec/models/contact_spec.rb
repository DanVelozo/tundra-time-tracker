require 'rails_helper'

RSpec.describe Contact, type: :model do
  let!(:user)  { FactoryBot.create(:user) }
  let(:client) { FactoryBot.create(:client, user: user) }
  
  describe 'factory' do
    it 'has a valid factory' do
      expect(FactoryBot.build(:contact)).to be_valid
    end
  end

  describe 'associations' do
    it { should belong_to(:client) }
    it { should belong_to(:user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:email) }
  end

  describe 'Attributes' do
    it 'is valid with a first_name and email' do
      contact = Contact.new(
        first_name: 'Pedro',
        email: 'pedro@teste.com',
        user: user,
        client: client
      )
      expect(contact).to be_valid
    end

    it 'is not valid without first_name and email' do
      contact = Contact.new(
        first_name: nil,
        email: nil,
        user: user,
        client: client
      )
      expect(contact).to_not be_valid
    end

    it 'is not valid without client associated' do
      contact = Contact.new(
        first_name: 'Carlos',
        email: 'carlos@teste.com',
        user: user,
        client: nil
      )
      expect(contact).to_not be_valid
    end
  end
end
require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:user) { FactoryBot.create(:user) }

  describe 'factory' do
    it 'has a valid factory' do
      expect(FactoryBot.build(:task)).to be_valid
    end
  end

  describe 'associations' do
    it { should have_many(:timesheets) }
    it { should belong_to(:user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:billable) }
  end

  describe 'Attributes' do
    it 'is valid with a name, hourly_rate, billable and user' do
      task = Task.new(
        name: 'Desenvolvimento',
        hourly_rate: 45,
        billable: true,
        user: user
      )
      
      expect(task).to be_valid
    end

    it 'is not valid without a name and billable' do
      task = Task.new(
        name: nil,
        hourly_rate: 45,
        billable: nil,
        user: user
      )

      expect(task).to_not be_valid
    end
  end
end
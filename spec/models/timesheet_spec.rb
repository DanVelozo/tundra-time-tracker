require 'rails_helper'

RSpec.describe Timesheet, type: :model do
  let!(:user)  { FactoryBot.create(:user) }
  let(:client)  { FactoryBot.create(:client, user: user) }
  let(:project) { FactoryBot.create(:project, user: user, client: client) }
  let(:task) { FactoryBot.create(:task, user: user) }

  describe 'factory' do
    it 'has a valid factory' do
      expect(FactoryBot.build(:timesheet)).to be_valid
    end
  end

  describe 'associations' do
    it { should belong_to(:project) }
    it { should belong_to(:task) }
    it { should belong_to(:user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:start_date) }
  end

  describe 'Attributes' do
    it 'is valid with a description, start_date, project, user and task' do
      timesheet = Timesheet.new(
        description: 'Tarefa XPTO',
        start_date: '15/05/2020',
        user: user,
        project: project,
        task: task
      )
      
      expect(timesheet).to be_valid
    end

    it 'is not valid without a description and start_date' do
      timesheet = Timesheet.new(
        description: nil,
        start_date: nil,
        user: user,
        project: project,
        task: task
      )

      expect(timesheet).to_not be_valid
    end
  end
end
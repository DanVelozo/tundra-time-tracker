FactoryBot.define do
  factory :project do
    name { 'Projeto XPTO' }
    project_code { 'xpto2020' }
    starts_on { '10/04/2020' }
    ends_on { '10/08/2020' }
    total_hours { '160' }
    description { 'Projeto Teste' }
    user
    client
  end
end

FactoryBot.define do
  factory :user do
    first_name { 'Test' }
    last_name  { 'User' }
    company_name { 'Company X' }
    email { 'test.user@teste.com' }
    password { '123456' }
  end
end

FactoryBot.define do
  factory :timesheet do
    description { 'Desenvolvimento da Tarefa XYZ' }
    start_date { '15/05/2015' }
    work_from { '08:00' }
    work_to { '18:00' }
    external_url { 'http://www.trello.com' }
    project
    task
    user
  end
end

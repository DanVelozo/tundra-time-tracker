FactoryBot.define do
  factory :task do
    name { 'Desenvolvimento' }
    hourly_rate { 45 }
    billable { true }
    user
  end
end

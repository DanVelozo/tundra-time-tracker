FactoryBot.define do
  factory :client do
    name { 'Tundra' }
    currency_type { 'R$ BRL' }
    address { 'Rua dos Testes 12345' }
    user
  end
end


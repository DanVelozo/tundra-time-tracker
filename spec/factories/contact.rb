FactoryBot.define do
  factory :contact do
    first_name { 'João' }
    last_name  { 'Silva' }
    email { 'joao.silva@teste.com' }
    office_number { '1132345678' }
    mobile_number { '11993459456' }
    user
    client
  end
end


class AddClientToProjects < ActiveRecord::Migration[5.1]
  def change
    add_reference :projects, :client, index: true
  end
end

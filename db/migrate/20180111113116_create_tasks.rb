class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string  :name
      t.decimal :hourly_rate
      t.boolean :billable
      t.timestamps
    end
  end
end

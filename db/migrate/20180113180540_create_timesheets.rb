class CreateTimesheets < ActiveRecord::Migration[5.1]
  def change
    create_table :timesheets do |t|
      t.text    :description
      t.string  :start_date
      t.string  :work_from
      t.string  :work_to
      t.integer :duration
      t.string  :external_url
      t.timestamps
    end
  end
end

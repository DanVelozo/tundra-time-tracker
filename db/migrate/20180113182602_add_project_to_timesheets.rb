class AddProjectToTimesheets < ActiveRecord::Migration[5.1]
  def change
    add_reference :timesheets, :project, index: true
  end
end

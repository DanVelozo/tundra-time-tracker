class AddUserToContacts < ActiveRecord::Migration[5.1]
  def change
    add_reference :contacts, :user, index: true
  end
end

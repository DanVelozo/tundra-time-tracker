class AddClientToContacts < ActiveRecord::Migration[5.1]
  def change
    add_reference :contacts, :client, index: true
  end
end

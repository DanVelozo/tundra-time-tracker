class ChangeDurationToBeDecimalInTimesheets < ActiveRecord::Migration[5.1]
  def change
    change_column :timesheets, :duration, :decimal, precision: 10, scale: 2, default: 0.0
  end
end

class AddTaskToTimesheets < ActiveRecord::Migration[5.1]
  def change
    add_reference :timesheets, :task, index: true
  end
end

puts "Creating User"

user = User.create(
    first_name: 'Test', 
    last_name: 'User', 
    company_name: 'Test Company', 
    email: 'test@test.com',
    password: '123456'
  )

puts "User email: #{user.email} | password: #{user.password} created successfully!"

puts "\n"

#####################################

puts "Creating Client"

client = Client.create(
    name: 'Client Test',
    currency_type: 'R$ BRL',
    user: user
  )

puts "Client name: #{client.name} created successfully!"

puts "\n"

#####################################

puts "Creating Contact"

contact = Contact.create(
    first_name: 'Test',
    last_name: 'Contact',
    email: 'contact@test.com',
    office_number: '1132345456',
    mobile_number: '11994566782',
    client: client,
    user: user
  )

puts "Contact first_name: #{contact.first_name} | last_name: #{contact.last_name} | email: #{contact.email} created successfully!"

puts "\n"

#####################################

puts "Creating Task"

task = Task.create(
    name: 'Task Test',
    hourly_rate: 45,
    billable: true
  )

puts "Task name: #{task.name} created successfully!"

puts "\n"

#####################################


puts "Creating Project"

project = Project.create(
    name: 'Test Project',
    project_code: '#test_project',
    starts_on: '01/05/2020',
    ends_on: '10/05/2020',
    total_hours: '168'
    description: 'Test Project XYZ' 
  )

puts "Project name: #{project.name} created successfully!"

puts "\n"

puts "You're all set!"